import matplotlib.pyplot as plt
import random
import math
import numpy as np

a=0
b=10
n=50
m=20000
binai=10000
Y0=6
Y=[]
for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  Y.append(y/n)

mu=np.average(Y)
s=np.std(Y)

bp=np.linspace(a, b, binai+1)

Yd=np.digitize(Y, bp)
dn=[]
dx=[]

for i in range(1, len(bp)):
	dn.append(len(Yd[Yd==i])/(m*mu))
	dx.append((bp[i-1]+bp[i])/2)

dA=np.multiply(dx,dn)
A=np.sum(dA)

pv=0
L=0

while dx[L]<=Y0:
	pv=pv+dA[L]
	L=L+1
P=1-pv
print("P-value:", P)
