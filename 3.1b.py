import matplotlib.pyplot as plt
import random
import math
import numpy as np
a=0
b=10
n=50
m=20000
binai=10000
CI1=68
CI2=95
CI3=99
Y=[]
for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  Y.append(y/n)

v=np.ones_like(Y)/float(len(Y))
plt.hist(Y, bins=100, weights=v)

mu=np.average(Y)
s=np.std(Y)
print("the mean:",mu)

bp=np.linspace(a, b, binai+1)

Yd=np.digitize(Y, bp)
dn=[]
dx=[]

for i in range(1, len(bp)):
	dn.append(len(Yd[Yd==i])/(m*mu))
	dx.append((bp[i-1]+bp[i])/2)

dA=np.multiply(dx,dn)
A=np.sum(dA)

Ai=0
L=0
galu1=(1-CI1/100)/2 #
while Ai/A<galu1:
	Ai=Ai+dA[L]
	L=L+1
Y1_1=dx[L]
Ai=0
L=0
while Ai/A<galu1:
	Ai=Ai+dA[L]
	L=L+1
Y2_1=dx[len(dA)-L-1]


Ai=0
L=0
galu2=(1-CI2/100)/2 #
while Ai/A<galu2:
	Ai=Ai+dA[L]
	L=L+1
Y1_2=dx[L]
Ai=0
L=0
while Ai/A<galu2:
	Ai=Ai+dA[L]
	L=L+1
Y2_2=dx[len(dA)-L-1]


Ai=0
L=0
galu3=(1-CI3/100)/2 #
while Ai/A<galu3:
	Ai=Ai+dA[L]
	L=L+1
Y1_3=dx[L]
Ai=0
L=0
while Ai/A<galu1:
	Ai=Ai+dA[L]
	L=L+1
Y2_3=dx[len(dA)-L-1]


print("the CI of  68% certainty is [", "%.4f" %Y1_1, ",", "%.4f" %Y2_1, "]")
print("the CI of  95% certainty is [", "%.4f" %Y1_2, ",", "%.4f" %Y2_2, "]")
print("the CI of  99% certainty is [", "%.4f" %Y1_3, ",", "%.4f" %Y2_3, "]")

plt.xlabel('Y')
plt.ylabel('pmf')
plt.savefig("histogram.1b.png")
plt.show()

