import matplotlib.pyplot as plt
import random
import math
import numpy as np

a1=0
b1=10
n1=50
m1=20000
binai=10000
Y0=6
Y=[]
for i in range(0,m1):
  y=0
  for i in range(0,n1):
   x=random.uniform(a1, b1)
   y=y+x
  Y.append(y/n1)
mu1=np.average(Y)
s1=np.std(Y)

bp1=np.linspace(a1, b1, binai+1)
Yd=np.digitize(Y, bp1)

dn1=[]
dx1=[]

for i in range(1, len(bp1)):
	dn1.append(len(Yd[Yd==i])/(m1*mu1))
	dx1.append((bp1[i-1]+bp1[i])/2)

dS1=np.multiply(dx1,dn1)
S1=np.sum(dS1)

a=-2
b=16
n=50
m=50000
binai=10000
A0=6
A=[]
for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  A.append(y/n)

mu=np.average(A)
s=np.std(A)

bp=np.linspace(a, b, binai+1)

Ad=np.digitize(A, bp)
dn=[]
dx=[]

for i in range(1, len(bp)):
	dn.append(len(Ad[Ad==i])/(m*mu))
	dx.append((bp[i-1]+bp[i])/2)

dS=np.multiply(dx,dn)
S=np.sum(dS)

q1=0
if Y0<mu1:
	L=0
	while dx1[L]<=Y0:
		q1=q1+dS1[L]
		L=L+1
else:
	L=len(dx1)-1
	while dx1[L]>=Y0:
		q1=q1+dS1[L]
		L=L-1

print(S1)
print(q1)

q=0
if Y0<mu:
	L=0
	while dx[L]<=Y0:
		q=q+dS[L]
		L=L+1
else:
	L=len(dx)-1
	while dx[L]>=Y0:
		q=q+dS[L]
		L=L-1
CL1=(S1-q1)*100
CL2=(S-q)*100

print("CL for H0:", "%.2f" %CL1, "%")
print("CL for HA:", "%.2f" %CL2, "%")


v1=np.ones_like(Y)/float(len(Y))
v=np.ones_like(A)/float(len(A))
plt.hist(Y, bins=100, weights=v1)
plt.hist(A, bins=100, weights=v)
plt.xlabel('Y (blue) and A (orange)')
plt.ylabel('pmf')
plt.savefig("histogram.2b.png")
plt.show()
