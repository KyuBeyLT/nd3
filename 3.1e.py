import matplotlib.pyplot as plt
import random
import math
import numpy as np

a=0
b=10
n=50
m=20000
binai=10000
Y=[]
Q=np.array([2.5, 16, 84, 97.5])

for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  Y.append(y/n)

mu=np.average(Y)
s=np.std(Y)

Yn=np.random.normal(5, 1/6, m)

bp=np.linspace(a, b, binai+1)

Yd=np.digitize(Y, bp)
dn=[]
dx=[]

for i in range(1, len(bp)):
	dn.append(len(Yd[Yd==i])/(m*mu))
	dx.append((bp[i-1]+bp[i])/2)

dA=np.multiply(dx,dn)
A=np.sum(dA)
Qn=[]


YnN=np.sort(Yn)

for i in range(len(Q)):
	Qn.append(YnN[int(Q[i]*len(YnN)/100)])
#	Qn = np.append(Qn, YnN[int(Q[i]*len(YnN)/100)])
prob=[]

for i in range(len(Qn)):
	L=0
	while Qn[i]>dx[L]:
		L=L+1
	galas=0
	for n in range(L):
		galas=galas+dA[n]
	prob.append(galas)

prob.append(A)
errgal=0
Qgal=np.append(Q,100)

for i in range(len(prob)):
	if i==0:
		Ppmf=prob[i]
		Pnor=Qgal[i]/100
	else:
		Ppmf=prob[i]-prob[i-1]
		Pnor=(Qgal[i]-Qgal[i-1])/100
	errgal=errgal+(Pnor-Ppmf)**2
err=errgal/(len(prob))
print("err:", "%.5f" %err)

