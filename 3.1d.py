mu = (a+b) / 2
sigma = (a-b)^2 /12

mu = 10 / 2 = 5

s^2 = sigma^2 / n          (s^2 is our sample variance)
s^2 = 10^2 / 12 / 50 = 1/6

f(x) = 1 / sqrt(2*pi*s^2) * exp(-1/2*( (x-mu)/s )^2)

I = integral f(x) from Y0 to +inf       (Y0 == 6)
I = 0.0072


Theoretical P-value is close to experimental data
