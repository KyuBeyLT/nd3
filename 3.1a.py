import matplotlib.pyplot as plt
import random
import numpy as np
import math

a=0
b=10
n=50
m=20000
binai=100 #bins sk

Y=[]
for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  Y.append(y/n)
plt.hist(Y, bins=binai)
plt.xlabel('Y')
plt.ylabel('number of events')
plt.savefig("histogram.1a.png")
plt.show()
