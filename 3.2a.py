import matplotlib.pyplot as plt
import random
import math
import numpy as np

a=-2
b=16
n=50
m=50000
binai=10000
A0=6
A=[]
for i in range(0,m):
  y=0
  for i in range(0,n):
   x=random.uniform(a, b)
   y=y+x
  A.append(y/n)

mu=np.average(A)
s=np.std(A)

bp=np.linspace(a, b, binai+1)

Ad=np.digitize(A, bp)
dn=[]
dx=[]

for i in range(1, len(bp)):
	dn.append(len(Ad[Ad==i])/(m*mu))
	dx.append((bp[i-1]+bp[i])/2)

dS=np.multiply(dx,dn)
S=np.sum(dS)

pv=0
L=0

while dx[L]<=A0:
	pv=pv+dS[L]
	L=L+1
print("P-value:", pv)

plt.hist(A, bins=100)
plt.xlabel('A')
plt.ylabel('number of events')
plt.savefig("histogram.2a.png")
plt.show()
